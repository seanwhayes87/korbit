from app.algos import count_groups


def test_one_large_group_all_filled():
    test_data = [
        [1, 1, 1, 1],
        [1, 1, 1, 1],
        [1, 1, 1, 1],
        [1, 1, 1, 1],
    ]
    actual = count_groups(test_data)
    assert actual == 1


def test_one_large_group_with_single_trailing_case():
    test_data = [
        [1, 1, 1, 1],
        [1, 1, 1, 1],
        [1, 1, 0, 0],
        [1, 1, 0, 1],
    ]
    actual = count_groups(test_data)
    assert actual == 1


def test_matrix_too_small():
    test_data = [
        [1],
    ]
    actual = count_groups(test_data)
    assert actual == 0


def test_two_groups():
    test_data = [
        [1, 1, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 1, 1],
        [0, 0, 1, 1],
    ]
    actual = count_groups(test_data)
    assert actual == 2


def test_no_groups_with_two_single_cases():
    test_data = [
        [1, 0, 0, 1],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [1, 0, 0, 1],
    ]
    actual = count_groups(test_data)
    assert actual == 0
