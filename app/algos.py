def flood_fill(matrix, row, col):
    if row >= len(matrix) or col >= len(matrix[0]) or matrix[row][col] == 0:
        return 0

    matrix[row][col] = 0

    flood_fill(matrix, row, col + 1)
    flood_fill(matrix, row + 1, col)


def count_groups(matrix):
    if not matrix or len(matrix) == 0:
        return 0

    count = 0
    for i, row in enumerate(matrix):
        for j, col in enumerate(row):
            # ensure we're not counting a single case in the bottom right of the group
            if i < len(matrix) - 1 and j < len(matrix[0]) - 1 and matrix[i][j] == 1:
                if matrix[i][j + 1] == 1 or matrix[i + 1][j] == 1:
                    count += 1
                    flood_fill(matrix, i, j)
    return count
