#!/bin/env python
import json

from flask import Blueprint, Flask, request

from app.algos import count_groups
from app.db import db

bp = Blueprint("covid", __name__)


@bp.route("/groups", methods=["POST"])
def get_groups():
    body = request.get_json()
    groups = count_groups(body.get("matrix"))

    # TODO: save matrix and groups to database

    return json.dumps({"count": groups})


@bp.route("/groups/time")  # TODO add parameters
def get_groups_by_datetime():
    return ""
