from app import db


class Matrix(db.model):
    id = db.Column(db.Integer, primary_key=True)

    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    num_groups = db.Column(db.Integer)
